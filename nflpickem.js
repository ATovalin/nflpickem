gamesList = new Mongo.Collection('gamesList');
usersPicks = new Mongo.Collection('usersPicks');
usersResults = new Mongo.Collection('usersResults');
matchResults = new Mongo.Collection('matchResults');
usersProfile = new Mongo.Collection('usersProfile');
weeklyUResults = new Mongo.Collection('weeklyUResults');

if (Meteor.isClient) {
    Session.set('gameMaster', "");
    Session.set('resultsInput', "");
    Session.set('gameResults', "");
    Session.set('weeklyStandings', "");
    Session.set('usersProfile', "");

    Router.route('/', function () {
        this.render('mainPage');
    });

    Router.route('/gameMaster', function () {
        this.render('gameMaster');
    });
    
    Router.route('/resultsInput', function() {
        this.render('/resultsInput');
    });
    
    Router.route('/gameresults', function() {
        this.render('/gameResults');
    });
    
    Router.route('/weeklyStandings', function() {
        this.render('/weeklyStandings');
    });
    
    Router.route('/usersProfile', function() {
        this.render('/usersProfile');
    });
    
    Accounts.ui.config({
        passwordSignupFields: "USERNAME_ONLY"
    });
    
    Template.mainPage.events({
        'click input': function(event) {
            var usersPickVar = event.currentTarget.defaultValue;
            var currentUserId = Meteor.userId();
            var week = event.currentTarget.className;    
            usersPicks.insert({
                weekNum: week,
                usersPick: usersPickVar,
                userID: currentUserId
            });
            console.log("User Pick Submitted");
            console.log(usersPickVar);
            console.log(week);
        },
        'click #gameResults' : function() {
            Router.go('/gameResults');
        },
        'click #weeklyStandings' : function() {
            Router.go('/weeklyStandings');
        },
        'click #usersProfile' : function() {
            Router.go('/usersProfile');
        }
    });
    
    Template.mainPage.helpers({
        'checkCurrent': function() {
            if (Meteor.user().username == "GameMaster") {
                Router.go('/gameMaster');
            } else {
                Router.go('/');
                console.log("Not gameMaster");
            }
        },
        'getGames': function() {
            return  gamesList.find().fetch();
        },
        'gameChosen': function() {
            if (Date.getDay() >= 4 && Date.getDay() !== 0) {
                console.log("true");
                return true;
            } else {
                return false;
            }
        }
    });
        
    Template.gameMaster.events({
        'submit #gameSubmit': function (event) {
            event.preventDefault();
            var team1 = event.target.team1.value;
            var team2 = event.target.team2.value;
            var week = event.target.week.value;
            gamesList.insert({
                team1: team1,
                team2: team2,
                weekNum: week
            });
            console.log("Game submitted");
        },
        'click #resultsInput' : function() {
            Router.go('/resultsInput');
        }
    });
    
    Template.resultsInput.events({
        'click input': function(event) {
            var winnerVar = event.currentTarget.defaultValue;
            var gameId = event.currentTarget.className;
            var week = getWeek(gameId);
            matchResults.insert({
                gameId: gameId,
                winner: winnerVar,
                weekNum: week
            });
            console.log("Winner Submitted");
            console.log(winnerVar);
        }
    });
    
    Template.resultsInput.helpers({
        'getGames' : function () {
            return gamesList.find().fetch();
        }
    });
    
    Template.gameResults.events({
        'submit #gameResults': function (event) {
            event.preventDefault();
            Session.set("selectedWeek", event.target.week.value);
        }
    });
    
    Template.gameResults.helpers({
        'getWinners' : function () {
            if (Session.get("selectedWeek")) {
                console.log("week");
                var week = Session.get("selectedWeek");
                var check = matchResults.find({weekNum: {'weekNum': week }}).fetch();
                return check;
            } else {
                console.log("not week");
            }
        },
        'getPicks' : function () {
            if (Session.get("selectedWeek")) {
                console.log("week");
                var week = Session.get("selectedWeek");
                return usersPicks.find({weekNum: week}).fetch();
            } else {
                console.log("not week");
            }
        }
    });
    
    Template.weeklyStandings.events({
        'submit #weeklyStandings': function (event) {
            event.preventDefault();
            Session.set("selectedWeekStanding", event.target.week.value);
        }
    });
    
    Template.weeklyStandings.helpers({
        'userStanding' : function () {
            if (Session.get("selectedWeekStanding")) {
                console.log("week selected");
                var week = Session.get("selectedWeekStanding");
                var currentUserId = Meteor.userId();
                exist = checkStandingExistence();
                            
                if (exist == "1") {
                    console.log(week);
                    return weeklyUResults.find({weekNum: week}).fetch();
                } else {
                    console.log("nonexistent");
                    insertStandings();//insert
                }
            } else {
                console.log("week not selected");
            }
        },
        'allStandings' : function () {
            if (Session.get("selectedWeekStanding")) {
                console.log("Week selected");
                var week = Session.get("selectedWeekStanding");
                return usersPicks.find({weekNum: week}).fetch();
            } else {
                console.log("Week not selected");
            }
        }
    });
    
    Template.usersProfile.events({
        'submit #profileUpload': function (event) {
            event.preventDefault();
            var currentUserId = Meteor.userId();
            var favoriteTeam = event.target.favTeam.value;

            usersProfile.insert({
                userId: currentUserId,
                wins: 0,
                losses: 0,
                ProfilePic: null,
                favTeam: favoriteTeam,
            });
            console.log("Uploaded");
        }
    });
    
    Template.usersProfile.helpers({
        'checkProfile' : function () {
            var currentUserId = Meteor.userId();
            temp = usersProfile.find({userId: currentUserId}).fetch();
            if (temp) {
                console.log('true');
                return true;
            } else {
                console.log('false');
                return false;
            }
        },
        'getProfile' : function () {
            userId = Meteor.userId();
            return usersProfile.find({userId: userId}).fetch();
        }
    });    
    
     
////////////////////////////////////////////////////////////////////////////    
    function getWeek (gameId) { //used for getting week for gameResults
        return week = gamesList.find({_id: gameId},{fields: {'weekNum': 1, _id:0}}).fetch();
    }
    
    function checkStandingExistence () {
        var id = Meteor.userId();
        return exist = weeklyUResults.find({userId: id}).count();
    }
    
    function insertStandings () {
        var id = Meteor.userId();
        //var results = checkResults();
        var week = Session.get("selectedWeekStanding");
        console.log('Hello?');
        //var wins = checkWins();
        //var losses = checkLosses(Wins);
        
        weeklyUResults.insert({
            userId: id,
            wins: wins,
            losses: losses,
            weekNum: week 
        });
    }
    
    function checkLosses () {
        var week = Session.get("selectedWeekStanding");
        var userpicks = usersPicks.find({weekNum: week}).count();
        var matchResults = matchResults.find({weekNum: week}).count();
        
        var losses = matchResults - userpicks;
        return losses;
    }
    
    function checkWins() {
        console.log("checkWins");
    }
}

